#include <iostream>
using namespace std;

struct node{
 int data;
 struct node* left;
 struct node* right;
};
struct node* newNode(int data){
 struct node* node = new struct node;
 node->data = data;
 node->left = NULL;
 node->right = NULL;
 return node;
}
void insert(struct node** rootRef, int data){
 if(*rootRef == NULL){
  *rootRef = newNode(data);
 }
 else{
  if(data <= (*rootRef)->data){
   insert(&((*rootRef)->left), data);
  }
  else{
   insert(&((*rootRef)->right), data);
  }
 }
}
void printInOrder(struct node* root){
 if(root != NULL){
  printInOrder(root->left);
  cout << root->data << " ";
  printInOrder(root->right);
 }
}
void printPreOrder(struct node* root){
 if(root != NULL){
  cout << root->data << " ";
  printPreOrder(root->left);
  printPreOrder(root->right);
 }
}
void printPostOrder(struct node* root){
 if(root != NULL){
  printPostOrder(root->left);
  printPostOrder(root->right);
  cout << root->data << " ";
 }
}
//Question 1: Find the size
int size(struct node* root){
 static int* retVal = new int;
 if(root != NULL){
  *retVal = *retVal + 1;
  size(root->left);
  size(root->right);
 }
 return *retVal;
}
//Question 2: Find the height
int height(struct node* root){
 static int* retVal = new int;
 if(root != NULL){
  *retVal = *retVal + 1;
  int leftHeight = height(root->left);
  int rightHeight = height(root->right)-leftHeight;
  if(leftHeight > rightHeight){
   *retVal = leftHeight + 1;
  }
  else{
   *retVal = rightHeight + 1;
  }
 }
 return *retVal;
}
//Question 3: Find a specific value
bool findVal(struct node* root, int val){
 static bool* retVal = new bool;
 *retVal = false;
 if(root != NULL){
  if(root->data == val){
   *retVal = true;
  }
  else{
   if(root->data > val){
    findVal(root->left, val);
   }
   else{
    findVal(root->right, val);
   }
  }
 }
 return *retVal;
}
//Question 4: Find the min and max values
int findMin(struct node* root){
 static int* retVal = new int;
 if(root != NULL){
  if(root->left == NULL){
   *retVal = root->data;
  }
  else{
   findMin(root->left);
  }
 }
 return *retVal;
}
int findMax(struct node* root){
 static int* retVal = new int;
 if(root != NULL){
  if(root->right == NULL){
   *retVal = root->data;
  }
  else{
   findMax(root->right);
  }
 }
 return *retVal;
}


int main(){
 struct node* root = NULL;

 insert(&root, 70);
 insert(&root, 90);
 insert(&root, 60);
 //Added when finding height
 insert(&root, 80);
 insert(&root, 100);
 insert(&root, 120);
 //Added when finding max and min
 insert(&root, 50);
 insert(&root, 110);

 cout << "In order: ";
 printInOrder(root);
 cout << endl;

 cout << "Pre order: ";
 printPreOrder(root);
 cout << endl;

 cout << "Post order: ";
 printPostOrder(root);
 cout << endl;

 cout << "Size: " << size(root) << endl;
 cout << "Height: " << height(root) << endl;
 cout << "Is 70 in the tree? " << findVal(root, 70) << endl;
 cout << "Is 80 in the tree? " << findVal(root, 80) << endl;
 cout << "Is 60 in the tree? " << findVal(root, 60) << endl;
 cout << "Is 45 in the tree? " << findVal(root, 45) << endl;
 cout << "Minimum value: " << findMin(root) << endl;
 cout << "Maximum value: " << findMax(root) << endl; 
 return 0;
}
